#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Bash completion
if [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
fi

#source /usr/share/doc/pkgfile/command-not-found.bash
#source /etc/profile.d/autojump.bash

alias ls='ls --color=auto'
alias diff='diff --color=auto'
alias grep='grep --color=auto'

RED="\[$(tput setaf 1)\]"
GREEN="\[$(tput setaf 2)\]"
RESET="\[$(tput sgr0)\]"
GIT_PROMPT_ONLY_IN_REPO=1

export PS1="${RED}\u${GREEN}@\W$ ${RESET}"

export LESS=-R
export LESS_TERMCAP_mb=$'\E[1;31m'     # begin bold
export LESS_TERMCAP_md=$'\E[1;36m'     # begin blink
export LESS_TERMCAP_me=$'\E[0m'        # reset bold/blink
export LESS_TERMCAP_so=$'\E[01;44;33m' # begin reverse video
export LESS_TERMCAP_se=$'\E[0m'        # reset reverse video
export LESS_TERMCAP_us=$'\E[1;32m'     # begin underline
export LESS_TERMCAP_ue=$'\E[0m'        # reset underline
# and so on

man() {
    LESS_TERMCAP_md=$'\e[01;31m' \
    LESS_TERMCAP_me=$'\e[0m' \
    LESS_TERMCAP_se=$'\e[0m' \
    LESS_TERMCAP_so=$'\e[01;44;33m' \
    LESS_TERMCAP_ue=$'\e[0m' \
    LESS_TERMCAP_us=$'\e[01;32m' \
    command man "$@"
}

shopt -s autocd
shopt -s histappend
shopt -s cdspell # This will correct minor spelling errors in a cd command.
shopt -s histappend # Append to history rather than overwrite
shopt -s checkwinsize # Check window after each command
shopt -s dotglob # files beginning with . to be returned in the results of path-name expansion.


PATH=$PATH:$HOME/.local/bin
source ~/.bash-git-prompt/gitprompt.sh
# tabtab source for jhipster package
# uninstall by removing these lines or running `tabtab uninstall jhipster`
[ -f /home/aron/Documents/Justrocket/Projects/rydies/rydies-backend/node_modules/tabtab/.completions/jhipster.bash ] && . /home/aron/Documents/Justrocket/Projects/rydies/rydies-backend/node_modules/tabtab/.completions/jhipster.bash

export TERMINAL=gnome-terminal